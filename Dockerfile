FROM amazonlinux:2018.03

RUN yum update -y
RUN yum install -y wget gcc-c++ tar gzip make ncurses-devel

RUN wget "https://cmake.org/files/v3.10/cmake-3.10.0.tar.gz"
RUN tar -xvzf cmake-3.10.0.tar.gz

WORKDIR /cmake-3.10.0
RUN bash bootstrap
RUN gmake
RUN gmake install


RUN mkdir /download
WORKDIR /download

RUN mkdir /goroot
RUN mkdir /gopath

RUN wget "https://golang.org/dl/go1.14.6.linux-amd64.tar.gz"
RUN tar -xzf go1.14.6.linux-amd64.tar.gz
RUN mv go/* /goroot

ENV GOROOT=/goroot
ENV GOPATH=/gopath

COPY swig-3.0.12.tar.gz . 
RUN tar -zxf swig-3.0.12.tar.gz
WORKDIR /download/swig-3.0.12
RUN ls

RUN /bin/sh configure --without-pcre
RUN make
RUN make install

WORKDIR /root