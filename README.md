# TrueVolve AmazonLinux CPP and Golang builder
This repository can be used to build Golang projects that is dependent on underlying C++ projects. This is especially 
useful for building AWS Lamda friendly binaries.

* Swig 3.0
* CMake
* GoLang

# Why?

Mostly because of this error...
```
 /lib64/libm.so.6: version `GLIBC_2.29' not found
```
Your AWS Lamda function will be complaining about this if you built your CPP code in a different environment than where
the lambda function is invoked in. 

Some GoLang projects utilises CGo and swig mechanisms to invoke libraries written in 
C++. When doing this you can't just build your project in a Ubuntu environment and hope that it will work in the 
Lamda environment anymore because once you start utilising CGO then your project is likely to be tightly coupled to 
the GLIBC version of your build environment.

The easiest solution to this problem is simply to rebuild all of your sources natively within the AWS Linux environment
and that is what this container is for. 

  